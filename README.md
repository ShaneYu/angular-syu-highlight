angular-syu-highlight
=====================

This is a filter which will perform highlighting based on a given string, array of strings or a function.
Please see the usages section below to see how to use this filter.

_NOTE: This filter must be used with ng-bind-html or ng-bind-html-unsafe, it will not work otherwise._


Installation
------------

    $ bower install angular-syu-highlight


Usages
-------

  _Coming soon..._

  
License
-------

Licensed under the terms of the MIT License.