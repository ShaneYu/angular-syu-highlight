/**
 * Created by yus on 27/08/2014.
 *
 * Highlight Filter
 *
 * This filter will wrap found matches in test within span tags with an highlight class.
 * It is possible to highlight based on a single string, array of strings or a customer function.
 *
 * This should be used with ng-bind-html attribute; example usage below.
 *
 * Example Usages:
 *      <p ng-bind-html="'AngularJS just simply rocks!' | highlight:'AngularJS'"></p>
 *      <p ng-bind-html="'AngularJS just simply rocks!' | highlight:'AngularJS':'myHighlightClass'"></p>
 *      <p ng-bind-html="'AngularJS just simply rocks!' | highlight:['AngularJS', 'simply']'"></p>
 *      <p ng-bind-html="'AngularJS just simply rocks!' | highlight:MyHighlightFunction"></p>
 */

'use strict';

angular.module('syu.filters').filter('highlight', function ($sce) {
    return function (input, search, _class){

        if (!angular.isString(input)) return input;
        if (angular.isUndefined(search)) return input;

        var result = input || '';
        _class = _class || 'highlight';

        if (angular.isString(search)) {
            result = result.replace(new RegExp(search, 'gi'), '<span class="' + _class + '">$&</span>');
        }
        else if (angular.isArray(search)) {
            for (var i = 0; i < search.length; i++) {
                result = result.replace(new RegExp(search[i], 'gi'), '<span class="' + _class + '">$&</span>');
            }
        }
        else if (angular.isFunction(search)) {
            result = search(input, _class);
        }

        return $sce.trustAsHtml(result);
    };
});